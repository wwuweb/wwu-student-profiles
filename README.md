# WWU Student/Alumni Profiles

A Drupal Feature that implements a Student/Alumni Profile content type and a
View Pane for listing these profiles. Based on the existing “Huxley
Student-Alumni-Donor-Profiles” feature.

See the feature's module help page for information about styling and possible
extensions.
