<?php
/**
 * @file
 * wwu_student_profiles.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wwu_student_profiles_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'view_student_profiles';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Student Profiles';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'responsive_grid';
  $handler->display->display_options['style_options']['columns'] = '2';
  $handler->display->display_options['style_options']['wrapper_classes'] = 'views-responsive-grid views-responsive-grid-profiles';
  $handler->display->display_options['style_options']['default_classes'] = 1;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Thumnbail Image */
  $handler->display->display_options['fields']['field_thumnbail_image']['id'] = 'field_thumnbail_image';
  $handler->display->display_options['fields']['field_thumnbail_image']['table'] = 'field_data_field_thumnbail_image';
  $handler->display->display_options['fields']['field_thumnbail_image']['field'] = 'field_thumnbail_image';
  $handler->display->display_options['fields']['field_thumnbail_image']['label'] = '';
  $handler->display->display_options['fields']['field_thumnbail_image']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_thumnbail_image']['element_class'] = 'wwuzen-align-left';
  $handler->display->display_options['fields']['field_thumnbail_image']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_thumnbail_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_thumnbail_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_thumnbail_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_thumnbail_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_thumnbail_image']['settings'] = array(
    'image_style' => 'profile_thumbnail',
    'image_link' => 'content',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_class'] = 'profile-name';
  $handler->display->display_options['fields']['title']['element_label_type'] = '0';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  /* Field: Content: Program */
  $handler->display->display_options['fields']['field_program']['id'] = 'field_program';
  $handler->display->display_options['fields']['field_program']['table'] = 'field_data_field_program';
  $handler->display->display_options['fields']['field_program']['field'] = 'field_program';
  $handler->display->display_options['fields']['field_program']['label'] = '';
  $handler->display->display_options['fields']['field_program']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_program']['element_class'] = 'profile-program';
  $handler->display->display_options['fields']['field_program']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_program']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_program']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_program']['element_default_classes'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_type'] = 'div';
  $handler->display->display_options['fields']['body']['element_class'] = 'profile-body';
  $handler->display->display_options['fields']['body']['element_label_type'] = '0';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '200',
  );
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['element_type'] = 'div';
  $handler->display->display_options['fields']['view_node']['element_class'] = 'more-link';
  $handler->display->display_options['fields']['view_node']['element_label_type'] = '0';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['view_node']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'READ MORE';
  /* Sort criterion: Field: Last Name (field_last_name) */
  $handler->display->display_options['sorts']['field_last_name_value']['id'] = 'field_last_name_value';
  $handler->display->display_options['sorts']['field_last_name_value']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['sorts']['field_last_name_value']['field'] = 'field_last_name_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'profile' => 'profile',
  );

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $export['view_student_profiles'] = $view;

  return $export;
}
